package com.example.course.practica6;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class MainActivity extends ActionBarActivity {

    Button save,load;
    EditText nombre1,correo1,telefono1;
    String Nombre,Correo,Telefono;
    int data_block=100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        save = (Button)findViewById(R.id.SAVE);
        load = (Button)findViewById(R.id.LOAD);
        nombre1 = (EditText)findViewById(R.id.nombre);
        correo1 = (EditText)findViewById(R.id.correo);
        telefono1 = (EditText)findViewById(R.id.telefono);
        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    FileInputStream fis = openFileInput("text.txt");
                    InputStreamReader isr = new InputStreamReader(fis);
                    char[] data = new char[data_block];
                    String final_data = "";
                    int size;
                    try {
                        while ((size = isr.read(data)) > 0) {
                            String read_data = String.valueOf(data, 0, size);
                            final_data += read_data;
                            data = new char[data_block];
                        }
                        Toast.makeText(getBaseContext(), "Contacto:" + final_data, Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Nombre = nombre1.getText().toString();
                Correo = correo1.getText().toString();
                Telefono = telefono1.getText().toString();
                try {
                    FileOutputStream fou = openFileOutput("text.txt", MODE_WORLD_READABLE);
                    OutputStreamWriter osw = new OutputStreamWriter(fou);
                    try {
                        osw.write(Nombre);
                        osw.write(Correo);
                        osw.write(Telefono);
                        osw.flush();
                        osw.close();
                        Toast.makeText(getBaseContext(), "Contacto guardado", Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
